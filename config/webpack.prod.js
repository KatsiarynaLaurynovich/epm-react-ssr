const webpack = require('webpack');

module.exports = function(configDirs) {
  let prodConfig = Object.assign({}, require('./webpack.common')(configDirs));
  
  prodConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({ minimize: true }));

  console.log('\x1b[36m%s\x1b[0m', 'Building for production ...');

  return prodConfig;
};