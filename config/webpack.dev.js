const webpack = require('webpack');

module.exports = function(configDirs) {
  let devConfig = Object.assign({}, require('./webpack.common')(configDirs));

  console.log('\x1b[36m%s\x1b[0m', 'Building for development ...');

  //devConfig.devtool = 'eval-source-map';
  
  return devConfig;
};