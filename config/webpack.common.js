const path = require('path');

module.exports = (configDirs) => {
    return {
        entry: configDirs.APP_DIR + '/client/index.js',
        output: {
            filename: 'bundle.js',
            path: configDirs.BUILD_DIR,
            publicPath: '/'
        },

        module: {
            rules: [
                {
                    test: /\.js?$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    options: {
                        presets: [
                            'react',
                            'stage-0',
                            ['env', { targets: { browsers: ['last 2 versions'] }}]
                        ]
                    }
                },
                {
                    test: /\.scss$/,
                    use: ['style-loader', 'css-loader', 'sass-loader']
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    use: [
                      {
                        loader: 'file-loader',
                      },
                    ],
                  }
            ]
        },
        plugins: [],
        devServer: {
            historyApiFallback: true
        }
    };
}
