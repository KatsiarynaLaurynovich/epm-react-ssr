const path = require('path');

const APP_DIR = path.resolve(__dirname, './src');
const BUILD_DIR = path.resolve(__dirname, './public');

const configDirs = {
    BUILD_DIR: BUILD_DIR,
    APP_DIR: APP_DIR
  }

module.exports = (env) => {
    if (env === 'dev' || env === 'prod') {
        return require(`./config/webpack.${env}.js`)(configDirs);
    } else {
        console.log("Wrong webpack build parameter. Possible choices: 'dev' or 'prod'.")
    }
}