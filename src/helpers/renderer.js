import React from 'react';
import { renderToString } from 'react-dom/server';
import Root from '../client/components/Root';

export default () => {
    const content = renderToString(<Root />);

    return `
        <html>
            <head></head>
            <body>
                <div id='root'>${content}</div>
                <script src="bundle.js"></script>
            </body>
        </html>
    `;
};