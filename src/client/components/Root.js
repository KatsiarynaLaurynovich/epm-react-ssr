import React from 'react';
import FunctionalComponent from './FunctionalComponent';
import ClassComponent from './ClassComponent';
import ClassPureComponent from './ClassPureComponent';
import NoJSXComponent from './NoJSXComponent';
import './style.scss';
import img from '../assets/react.png';

const Root = () => {
    return (
        <div className={'container'}>
            <FunctionalComponent />
            <ClassComponent />
            <ClassPureComponent />
            <NoJSXComponent />
            <img src={img} />
        </div>
    )
};

export default Root;
