import React from 'react';

const NoJSXComponent = () => {
     return React.createElement(
        'div', 
        null, 
        'Hello World from React.createElement component'
    );
};

export default NoJSXComponent;
