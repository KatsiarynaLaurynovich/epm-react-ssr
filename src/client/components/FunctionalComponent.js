import React from 'react'; 

const FunctionalComponent = () => {
    return <div>Hello World from Functional Component!</div>
};

export default FunctionalComponent;
