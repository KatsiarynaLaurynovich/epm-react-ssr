import React, { Component } from 'react';

class ClassComponent extends Component {
    render () {
        return <div>Hello World from Class Component</div>
    }
}

export default ClassComponent;