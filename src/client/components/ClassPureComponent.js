import React, { PureComponent } from 'react';

class ClassPureComponent extends PureComponent {
    render () {
        return <div>Hello World from Class PureComponent</div>
    }
}

export default ClassPureComponent;